const container = document.querySelector(".container");
const meals = [
  {
    name: "Caramelizado",
    image: "images/caramelizado.jpg"
  },
  {
    name: "Turbinado",
    image: "images/turbinado.jpg"
  },
  {
    name: "Furioso",
    image: "images/furioso.jpg"
  },
  {
    name: "Smash",
    image: "images/smash.jpg"
  },
  {
    name: "Mini Smash",
    image: "images/smashzinho.jpg"
  },
  {
    name: "Combo Nairóbi",
    image: "images/combo1.jpg"
  },
  {
    name: "Combo Tokyo",
    image: "images/combo2.jpg"
  }
];
const showHamburgueres = () => {
  let output = "";
  meals.forEach(
    ({ name, image }) =>
      (output += `
              <div class="pag">
                <img class="pag--imagem" src=${image} />
                <h1 class="pag--title">${name}</h1>
                <a class="pag--link" href="#">Comer</a>
              </div>
              `)
  );
  container.innerHTML = output;
};

document.addEventListener("DOMContentLoaded", showHamburgueres);
